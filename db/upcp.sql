-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2015 a las 05:05:50
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `upcp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_nombre` varchar(100) NOT NULL,
  `con_valor` text NOT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `con_telefono` varchar(15) NOT NULL,
  `con_nombre` varchar(250) NOT NULL,
  `con_fecha` date NOT NULL,
  `con_hora` time NOT NULL,
  PRIMARY KEY (`con_telefono`),
  UNIQUE KEY `con_telefono` (`con_telefono`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar la informacion de usuario que envie desde la aplicacion de su celular la informacion que se necesite para contactarlo';

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`con_telefono`, `con_nombre`, `con_fecha`, `con_hora`) VALUES
('60856765', 'Luis Alberto Vega', '2014-11-07', '23:13:39'),
('75055369', 'Jose Manuel Peralta', '2014-11-07', '23:18:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `denuncia`
--

CREATE TABLE IF NOT EXISTS `denuncia` (
  `den_id` int(11) NOT NULL AUTO_INCREMENT,
  `den_asunto` varchar(200) NOT NULL,
  `den_descripcion` text NOT NULL,
  `den_fecha` date NOT NULL,
  PRIMARY KEY (`den_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE IF NOT EXISTS `noticia` (
  `not_id` int(11) NOT NULL AUTO_INCREMENT,
  `not_titulo` varchar(200) NOT NULL,
  `not_descripcion` text,
  `not_imagen` varchar(50) DEFAULT NULL,
  `not_fecha` date NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`not_id`, `not_titulo`, `not_descripcion`, `not_imagen`, `not_fecha`) VALUES
(1, 'nueva imagen', 'descripcion', 'default_upcp.png', '2014-11-08'),
(2, 'nueva noticia', 'tu noticia te informa que tu tienes q ser bueno en esto', 'default_upcp.png', '2014-11-08'),
(12, 'Noticia con app', '<b>Luis Mercado </b>2006322134 14<br><b>Jorge Menacho Toledo </b>2014205465 15<br><b>Efrain Fernadez Rojas</b>&nbsp; 2014524587 12<br>', 'default_upcp.png', '2014-11-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propuesta`
--

CREATE TABLE IF NOT EXISTS `propuesta` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_titulo` varchar(200) NOT NULL,
  `pro_descripcion` text NOT NULL,
  `pro_contacto_id` varchar(15) NOT NULL,
  `pro_fecha` date NOT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
