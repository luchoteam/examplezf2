<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => function ($sm)
            {
                $adapter = new \Zend\Db\Adapter\Adapter(array(
                    'driver' => 'Mysqli',
                    'database' => 'upcp',
                    'username' => 'root',
                    'password' => '',
                    'hostname' => 'localhost',
                    'profiler' => true,
                    'charset' => 'UTF8',
                    'options' => array(
                        'buffer_results' => true
                    )
                ));

                return $adapter;
            },
        ),
        'aliases' => array(
            'adapter' => 'Zend\Db\Adapter\Adapter'
        )
    ),
    'phpSettings' => array(
        'display_startup_errors' => false,
        'display_errors' => false,
        'max_execution_time' => 60,
        'date.timezone' => 'America/La_Paz',
        'mbstring.internal_encoding' => 'UTF-8'
    )
);
