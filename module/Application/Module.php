<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new \Zend\Mvc\ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array(
            $this,
            'beforeDispatch'
        ), 100);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array(
            $this,
            'afterDispatch'
        ), - 100);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Lib' => __DIR__ . '/../../vendor/Lib'
                )
            ),
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php'
            ]
        );
    }

    /**
     * Metodo que llama antes que se ejecute cualquier accion
     * del controlador llamado para que realice un operacion y validacion de inicio de sesion
     * de usuario en el caso que no se tenga sesion creada se redirecciona al formulario de logeo principal.
     *
     * @param MvcEvent $event
     */
    function beforeDispatch(MvcEvent $event)
    {
    	$request = $event->getRequest();
    	$response = $event->getResponse();
    	$target = $event->getTarget();

    	$whiteList = array(
    			'Application\Controller\Auth-login'
    	);

    	$allowedAccessList = [
    		'Application\Controller\Index',
    	];

    	$requestUri = $request->getRequestUri();
    	$controller = $event->getRouteMatch()->getParam('controller');
    	$action = $event->getRouteMatch()->getParam('action');

    	$requestedResourse = $controller . "-" . $action;

    	$session = new \Zend\Session\Container('User');

    	// Para verificar si inicio sesion correctamente
    	if ($session->offsetExists('username')) {
    		if ($requestedResourse == 'Application\Controller\Auth-login' || in_array($requestedResourse, $whiteList)) {
    			$url = '/anuncio/index';
    			$response->setHeaders($response->getHeaders()
    					->addHeaderLine('Location', $request->getBaseUrl() . $url));
    			$response->setStatusCode(302);
    		}
    	} else {

    		// Para el caso que se ejecute una accion que no tiene acceso permitido y no se inicio sesion.
    		if (! in_array($controller, $allowedAccessList)) {
    			if ($requestedResourse != 'Application\Controller\Auth-login' && ! in_array($requestedResourse, $whiteList)) {
    				$url = '/auth/login';
    				$response->setHeaders($response->getHeaders()
    						->addHeaderLine('Location', $request->getBaseUrl() . $url));
    				$response->setStatusCode(302);
    			}
    		}

    		$response->sendHeaders();
    	}
    }

    function afterDispatch(MvcEvent $event)
    {
    	// print "Called after any controller action called. Do any operation.";
    }
}
