<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Entity;

class Noticia extends \Core\Entity\AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $not_id;

    /**
     *
     * @var string
     */
    protected $not_titulo;

    /**
     *
     * @var string
     */
    protected $not_descripcion;

    /**
     * @var string
     */
    protected $not_fecha;

}
