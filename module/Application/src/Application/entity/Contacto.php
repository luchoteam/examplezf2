<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Entity;

class Contacto extends \Core\Entity\AbstractEntity
{

    /**
     *
     * @var string
     */
    protected $con_telefono;

    /**
     *
     * @var string
     */
    protected $con_nombre;

    /**
     *
     * @var string
     */
    protected $con_fecha;

    /**
     * @var string
     */
    protected $con_hora;

}
