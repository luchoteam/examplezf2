<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Entity;

class Denuncia extends \Core\Entity\AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $den_id;

    /**
     *
     * @var string
     */
    protected $den_asunto;

    /**
     *
     * @var string
     */
    protected $den_descripcion;

    /**
     * @var string
     */
    protected $den_fecha;

}
