<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Entity;

class Propuesta extends \Core\Entity\AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $pro_id;

    /**
     *
     * @var string
     */
    protected $pro_titulo;

    /**
     *
     * @var string
     */
    protected $pro_descripcion;

    /**
     * @var string
     */
    protected $pro_fecha;

    /**
     * @var string
     */
    protected $pro_contacto;

}
