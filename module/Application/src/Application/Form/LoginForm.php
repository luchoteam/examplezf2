<?php
/**
 * @since 2014-07-22 10:40 am
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Form;

use Zend\Form\Form;

/**
 * Formulario para la funcionalidad login de usuario
 * para el acceso al sistema.
 *
 * @author Luis Vega <lvega@alekia.com.bo>
 *
 */
class LoginForm extends Form {

    public function __construct($name = null) {

        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user',
            'type' => 'text',
            'options' => array(
                'label' => 'Usuario',
                'id' => 'user',
                'placeholder' => 'usuario.upcp'
            )
        ));

       $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'options' => array(
                'label' => utf8_encode('Contrasenia'),
                'id' => 'password',
                'placeholder' => '**********'
            )
       ));

       $this->add(array(
           'name' => 'submit',
           'attributes' => array(
               'type' => 'submit',
               'class' => 'btn btn-success',
               'value' => 'Iniciar Sesion',
           ),
       ));
    }
}