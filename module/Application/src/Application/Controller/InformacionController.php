<?php
/**
 * @since 2014-07-22 10:35 am
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Controller;

class InformacionController extends \Core\Controller\BaseController {

	public function contactoAction() {
		$contactoModel = $this->getServiceLocator()->get('\Application\Model\Informacion');
		$contactos = $contactoModel->getAllContacto();

		if ($contactos) {

			$this->assign('contactos', $contactos);
		} else {
			$this->setResponse(utf8_encode('No se encontro informacion para mostrar.'), 'error');
		}
		return $this->viewVars;
	}

	public function propuestaAction() {
		$propuestaModel = $this->getServiceLocator()->get('\Application\Model\Informacion');
		$propuestas = $propuestaModel->getAllPropuesta();
		$request = $this->getRequest();

		if ($request->isPost()) {
			$data = $request->getPost();
			$ids = $data->toArray();

			return $this->redirect()->toRoute('informacion', array(
					'action' => 'eliminarPropuesta'), ['query' => ['id' => $ids['id']]]);
		} elseif ($propuestas) {

			$this->assign('propuestas', $propuestas);
		} else {
			$this->setResponse(utf8_encode('No se encontro informacion para mostrar.'), 'error');
		}
		return $this->viewVars;
	}

	public function eliminarPropuestaAction() {
		$parameters = $this->getRequest()->getQuery();

		if ($parameters->id) {
			$informacionModel = $this->getServiceLocator()->get('\Application\Model\Informacion');
			$informacionModel->removePropuestas($parameters->id);
		}
		return $this->redirect()->toRoute('informacion', array(
				'action' => 'propuesta'
		));
	}

	public function eliminarDenunciaAction() {
		$parameters = $this->getRequest()->getQuery();

		if ($parameters->id) {
			$informacionModel = $this->getServiceLocator()->get('\Application\Model\Informacion');
			$informacionModel->removeDenuncias($parameters->id);
		}
		return $this->redirect()->toRoute('informacion', array(
				'action' => 'denuncia'
		));
	}

	public function denunciaAction() {
		$denunciaModel = $this->getServiceLocator()->get('\Application\Model\Informacion');
		$denuncias = $denunciaModel->getAllDenuncia();
		$request = $this->getRequest();

		if ($request->isPost()) {
			$data = $request->getPost();
			$ids = $data->toArray();

			return $this->redirect()->toRoute('informacion', array(
					'action' => 'eliminarDenuncia'), ['query' => ['id' => $ids['id']]]);
		} elseif ($denuncias) {

			$this->assign('denuncias', $denuncias);
		} else {
			$this->setResponse(utf8_encode('No se encontro informacion para mostrar.'), 'warning');
		}
		return $this->viewVars;
	}

}