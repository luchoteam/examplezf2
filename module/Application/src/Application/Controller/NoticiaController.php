<?php
/**
 * @since 2014-07-22 10:35 am
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Controller;

use Lib\ZebraImage;
class NoticiaController extends \Core\Controller\BaseController {

	public function indexAction() {
		$noticiaModel = $this->getServiceLocator()->get('\Application\Model\Noticia');
		$noticias = $noticiaModel->getAllNoticia();
		$request = $this->getRequest();

		if ($request->isPost()) {
			$data = $request->getPost();
			$ids = $data->toArray();

			return $this->redirect()->toRoute('noticia', array(
					'action' => 'eliminar'), ['query' => ['id' => $ids['id']]]);
		} elseif ($noticias) {

			$this->assign('noticias', $noticias);
		}
		return $this->viewVars;
	}

	public function eliminarAction() {
		$parameters = $this->getRequest()->getQuery();

		if ($parameters->id) {
			$noticiaModel = $this->getServiceLocator()->get('\Application\Model\Noticia');
			$noticiaModel->remove($parameters->id);
		}
		return $this->redirect()->toRoute('noticia', array(
				'action' => 'index'
		));
	}

	public function nuevoAction() {
		$request = $this->getRequest();

		if ($request->isPost()) {
			$data = $request->getPost();
			$files =  $request->getFiles()->toArray();
			$filesArray = array_values($files);
			$filesArray = $filesArray[0];
			$fileName = $filesArray['name'];
			$fileNameArray = explode('.', $fileName);
			$fileExtension = $fileNameArray[count($fileNameArray) - 1];
			$fileNameShaName = sha1('imagen_name_upcp' . mt_rand(10000, 999999)) . '.'. $fileExtension;
			$fileNameShaPath = IMAGE_PATH .'/'. $fileNameShaName;

			$uploadObj = new \Zend\File\Transfer\Adapter\Http();
			$uploadObj->setDestination(IMAGE_PATH);

			if($uploadObj->receive($fileName)) {
				$pathNameUpload = IMAGE_PATH . '/'. $fileName;
				rename($pathNameUpload, $fileNameShaPath);
				$data->not_imagen = $fileNameShaName;

				// imagen comprimida
				$pathImageCopy = IMAGE_PATH .'/comprimido/'. $fileNameShaName;
				copy($fileNameShaPath, $pathImageCopy);
				$zebraImage = new ZebraImage();
				$maxWidth = 120;
				$maxHeight = 90;
				// resize imagea
				$zebraImage->jpeg_quality = 100;
				$zebraImage->png_compression = 9;
				$zebraImage->preserve_aspect_ratio = true;
				$zebraImage->enlarge_smaller_images = false;
				$zebraImage->sharpen_images = false;
				$zebraImage->source_path = $pathImageCopy;
				$zebraImage->target_path = $pathImageCopy;
				$zebraImage->resize($maxWidth, $maxHeight, ZEBRA_IMAGE_NOT_BOXED);

			} else {
				$data->not_imagen = 'default_upcp.png';
			}

			$noticiaModel = $this->getServiceLocator()->get('\Application\Model\Noticia');
			$response = $noticiaModel->save($data);

			if ($response) {
				return $this->redirect()->toRoute('noticia', array(
						'action' => 'index'
				));
			} else {
				$this->setResponse(utf8_encode('Problema al almacenar noticia, pongase en contacto con el administrador de sistema.'), 'error');
			}
		}
		return $this->viewVars;
	}

	public function modificarAction() {
		// parametros GET
		$parameters = $this->getRequest()->getQuery();
		$request = $this->getRequest();
		if (!$request->isPost() && $parameters->noticia_id && is_numeric($parameters->noticia_id)) {
			$noticiaModel = $this->getServiceLocator()->get('\Application\Model\Noticia');
			$noticia = $noticiaModel->getNoticia($parameters->noticia_id);
			if ($noticia) {
				$this->assign('noticia', $noticia);
			} else {
				$this->setResponse('La url tiene errores no es valida', 'error');
				$this->assign('warning', true);
			}
		} elseif($request->isPost()) {
			$data = $request->getPost();
			$noticiaModel = $this->getServiceLocator()->get('\Application\Model\Noticia');
			$response = $noticiaModel->update($data);

			if ($response) {
				return $this->redirect()->toRoute('noticia', array(
						'action' => 'index'
				));
			} else {
				$this->setResponse(utf8_encode('Problema al actualizar noticia, pongase en contacto con el administrador de sistema.'), 'error');
				$this->assign('warning', true);
			}
		} else {
			$this->setResponse('La url tiene errores no es valida', 'error');
			$this->assign('warning', true);
		}

		return $this->viewVars;
	}
}