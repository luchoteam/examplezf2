<?php
/**
 * @since 2014-07-22 10:35 am
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Controller;

use Application\Form\LoginForm;

class AuthController extends \Core\Controller\BaseController
{

    /**
     * Accion para login de usuario e inicio de sesion de acuerdo a los parametros
     * enviados por POST :
     * @field string user - Nombre de la cuenta del usuario.
     * @field string password - Contrase�a de la cuenta del usuario.
     *
     * Esta informacion se hace validacion con informacion almacenada en archivos de configuracion
     * configurable en el caso que este todo correcto se inicia sesion, en otro caso se muestra un mensaje
     * de advertencia al usuario.
     */
    public function loginAction()
    {
        $request = $this->getRequest();

        $loginForm = new LoginForm();

        $loginForm->get('user')->setAttribute('id', 'user');
        $loginForm->get('user')->setAttribute('placeholder', 'user.upcp');
        $loginForm->get('password')->setAttribute('id', 'password');
        $loginForm->get('password')->setAttribute('placeholder', '******');
        if ($request->isPost()) {
            $data = $request->getPost();
            $auth = false;
            $config = $this->getServiceLocator()->get('\Application\Model\ModuleConfig');
            $configAuthUser = ($config->getAuthentication('user')) ? $config->getAuthentication('user') : null;
            $configAuthPassword = ($config->getAuthentication('password')) ? $config->getAuthentication('password') : null;

            if (isset($configAuthUser) && isset($configAuthPassword)) {
                if ($configAuthUser == $data->user && $configAuthPassword == $data->password) {
                    $bcrypt = new \Zend\Crypt\Password\Bcrypt();
                    $securedpassword = $bcrypt->create($configAuthUser);
                    $session = new \Zend\Session\Container('User');
                    $session->offsetSet('username', $configAuthUser);
                    $session->offsetSet('password', $securedpassword);

                    return $this->redirect()->toRoute('noticia', array(
                        'action' => 'index'
                    ));
                } else {
                    $this->setResponse(utf8_encode('Cuenta de usuario o Contrasenia incorrecto, pongase en contacto con el administrador de sistema.'), 'error');
                }
            } else {
                $this->setResponse(utf8_encode('Erro en configuracion de cuenta de usuario, pongase en contacto con el administrador de sistema.'), 'error');
            }
        }

        $this->assign('loginForm', $loginForm);

        return $this->viewVars;
    }

    /**
     * Metodo que elimina la sesion creada en el proceso de logeo de usuario
     * y redirecciona a la funcionalidad para logeo.
     */
    public function logoutAction()
    {
        $session = new \Zend\Session\Container('User');
        $session->getManager()->destroy();
        return $this->redirect()->toRoute('auth', array(
            'action' => 'login'
        ));
    }
}