<?php
/**
* @since 2014-05-27 15:07 pm
* @author Ronny Cuellar <rcuellar@alekia.com.bo>
* @copyright  Alekia SOLUCIONES S.R.L 2014
*/
namespace Application\Db;

class Noticia extends \Core\Db\AbstractDb
{

    /**
     *
     * @var string: El nombre de tabla a la que hace referencia en la Base de Datos.
     */
    protected $tableName = 'noticia';

    /**
     *
     * @var string: La clase entity que seteara para el cargado de datos de la Base de Datos.
     */
    protected $entityClassName = '\Application\Entity\Noticia';
}