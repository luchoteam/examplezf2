<?php
/**
* @since 2014-05-27 15:07 pm
*/
namespace Application\Db;

class Denuncia extends \Core\Db\AbstractDb
{

    /**
     *
     * @var string: El nombre de tabla a la que hace referencia en la Base de Datos.
     */
    protected $tableName = 'denuncia';

    /**
     *
     * @var string: La clase entity que seteara para el cargado de datos de la Base de Datos.
     */
    protected $entityClassName = '\Application\Entity\Denuncia';
}