<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Model;

/**
 * @author Luis Vega <lvega@alekia.com.bo>
 *
 */
class Noticia extends \Core\Model\AbstractModel
{

    /**
     *
     * @var \Application\Entity\Noticia
     */
    protected $noticia;

    /**
     *
     * @var \Application\Db\Noticia
     */
    protected $noticiaDb;

    /**
     * Retorna array de Entities \Application\Entity\Noticia
     *
     * @return array \Application\Entity\Noticia
     */
    public function getAllNoticia()
    {
        $noticiaDb = $this->getServiceLocator()->get('\Application\Db\Noticia');
        $noticias = $noticiaDb->findBy();

		if ($noticias->count() > 0) {
        	return $noticias;
		}
		return null;
    }

    public function save(\Zend\Stdlib\Parameters $data) {
    	$noticiaDb = $this->getServiceLocator()->get('\Application\Db\Noticia');
    	$fecha = date('Y-m-d');
    	$data->not_fecha = $fecha;
    	$dataArray = $data->toArray();
    	unset($dataArray['nuevo']);
    	$response = $noticiaDb->doInsert($dataArray);
        $noticiaId = $response->getGeneratedValue();
        if ($noticiaId) {
        	return true;
        }

        return false;
    }

    public function update(\Zend\Stdlib\Parameters $data) {
    	$noticiaDb = $this->getServiceLocator()->get('\Application\Db\Noticia');
    	$fecha = date('Y-m-d');
    	$data->not_fecha = $fecha;
    	$dataArray = $data->toArray();
    	unset($dataArray['actualizar']);
    	$response = $noticiaDb->doUpdate($dataArray, ['not_id' => $data->not_id]);
        if ($response) {
        	return true;
        }

        return false;
    }

    public function remove(array $ids) {
    	$noticiaDb = $this->getServiceLocator()->get('\Application\Db\Noticia');

    	foreach ($ids as $key => $id) {
    		$noticiaDb->doDelete(['not_id' => $id]);
    	}

        return true;
    }

    /**
     * @param unknown $noticiaId
     * @return \Application\Entity\Noticia | boolean
     */
    public function getNoticia($noticiaId) {
    	$noticiaDb = $this->getServiceLocator()->get('\Application\Db\Noticia');
    	$noticia = $noticiaDb->findBy(['not_id' => $noticiaId]);
    	$entity = $noticia->current();
    	if ($entity) {
    		return $entity;
    	}
    	return false;
    }
}
