<?php
/**
 * @since 2014-04-03 17:00 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Model;

/**
 * @author Luis Vega <lvega@alekia.com.bo>
 *
 */
class Informacion extends \Core\Model\AbstractModel
{

    /**
     * Retorna array de Entities \Application\Entity\contacto
     *
     * @return array \Application\Entity\Contacto
     */
    public function getAllContacto()
    {
        $contactoDb = $this->getServiceLocator()->get('\Application\Db\Contacto');
        $contactos = $contactoDb->findBy();

		if ($contactos->count() > 0) {
        	return $contactos;
		}
		return null;
    }

    /**
     * Retorna array de Entities \Application\Entity\Propuesta
     *
     * @return array \Application\Entity\Propuesta
     */
    public function getAllPropuesta()
    {
        $propuestaDb = $this->getServiceLocator()->get('\Application\Db\Propuesta');
        $propuestas = $propuestaDb->findBy();

		if ($propuestas->count() > 0) {
        	return $propuestas;
		}
		return null;
    }

    /**
     * Retorna array de Entities \Application\Entity\Denuncia
     *
     * @return array \Application\Entity\Denuncia
     */
    public function getAllDenuncia()
    {
        $denunciaDb = $this->getServiceLocator()->get('\Application\Db\Denuncia');
        $denuncias = $denunciaDb->findBy();

		if ($denuncias->count() > 0) {
        	return $denuncias;
		}
		return null;
    }

    public function removePropuestas(array $ids) {
    	$propuestaDb = $this->getServiceLocator()->get('\Application\Db\Propuesta');

    	foreach ($ids as $key => $id) {
    		$propuestaDb->doDelete(['pro_id' => $id]);
    	}

        return true;
    }

    public function removeDenuncias(array $ids) {
    	$denunciaDb = $this->getServiceLocator()->get('\Application\Db\Denuncia');

    	foreach ($ids as $key => $id) {
    		$denunciaDb->doDelete(['den_id' => $id]);
    	}

        return true;
    }

}
