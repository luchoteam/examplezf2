<?php
/**
 * @since 2014-05-26 15:30 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 */
namespace Application\Model;

use Core\Model\AbstractModel;

/**
 * Model creado para obtener los parametros de configuracion del archivo
 * \Application\config\module.config.php requerido de acuerdo
 * la necesidades especificas.
 *
 * @since 2014-05-26 15:30 pm
 * @author Luis Vega <lvega@alekia.com.bo>
 *
 */
class ModuleConfig extends AbstractModel
{

    const KEY_BOUNCED_SERVERS = 'servers_bounced';

    const KEY_SENDING_EMAIL = 'sending_email';

    const KEY_AUTHENTICATION = 'authentication';

    /**
     * Retorna valor de constantes de configuracion del archivo module.config,
     * se env�a el parametro $nameServer se retorna un array con sus valores.
     * de la constante en el array servers_bounced, cada elemento de este array
     * es una cuenta email, en caso que no encuentre valor retorna boolean false.
     *
     * @param string $nameParam
     * @return boolean | array
     */
    public function getServerBuncedParams($nameServer)
    {
        $arrayServers = (is_array($this->getServiceLocator()->get('config')[self::KEY_BOUNCED_SERVERS])) ? $this->getServiceLocator()->get('config')[self::KEY_BOUNCED_SERVERS] : [];
        if (isset($arrayServers[$nameServer])) {
            $params = $arrayServers[$nameServer];
            return $params;
        }

        return false;
    }

    /**
     * Retorna valor de constantes de configuracion del archivo module.config,
     * se env�a el parametro $key se retorna su valor en este caso retorna el valores
     * para la autenticacion se usuario para acceso al sistema, en caso que no encuentre valor retorna boolean false.
     *
     * @param string $key
     *            - Valor de la cantidad maxima para envio por minuto de boletines.
     * @return int | boolean
     */
    public function getAuthentication($key)
    {
        $arrayServers = (is_array($this->getServiceLocator()->get('config')[self::KEY_AUTHENTICATION])) ? $this->getServiceLocator()->get('config')[self::KEY_AUTHENTICATION] : [];
        if ($key && isset($arrayServers[$key])) {
            $value = $arrayServers[$key];
            return $value;
         }

        return false;
    }

    /**
     * Retorna valor de constantes de configuracion del archivo module.config,
     * se env�a el parametro $key se retorna su valor en este caso retorna el valores
     * para envio por minuto de boletines
     * de la constante en el array sending_email, en caso que no encuentre valor retorna boolean false.
     *
     * @param string $key
     *            - Valor de la cantidad maxima para envio por minuto de boletines.
     * @return int | boolean
     */
    public function getSendingEmail($key = 'account_sending')
    {

        $arrayServers = (is_array($this->getServiceLocator()->get('config')[self::KEY_SENDING_EMAIL])) ? $this->getServiceLocator()->get('config')[self::KEY_SENDING_EMAIL] : [];
        if ($key && isset($arrayServers[$key])) {
            $value = $arrayServers[$key];
            return $value;
         }

        return false;
    }
}