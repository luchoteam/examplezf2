<?php
namespace Core;

class Response extends \Zend\Stdlib\Message
{

    /**
     *
     * @param string $type
     *            (success|error|warning|info)
     * @return \Core\Communicator
     */
    function setType($type)
    {
        if ('error' === $type || (is_bool($type) && false === $type))
        {
            $type = 'danger';
        }
        elseif ('status' === $type || (is_bool($type) && true === $type))
        {
            $type = 'success';
        }
        
        $this->setMetadata('type', $type);
        return $this;
    }

    /**
     *
     * @return string
     */
    function getType()
    {
        return $this->getMetadata('type', 'status');
    }
}