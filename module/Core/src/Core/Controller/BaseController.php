<?php
namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class BaseController extends AbstractActionController
{

    /**
     *
     * @var message
     */
    protected $viewVars;

    /**
     *
     * @param strign|\Core\Response $reponse            
     * @param strign|bool $type            
     * @return \Core\Controller\BaseController
     */
    function saveResponse($reponse, $type = null)
    {
        $session = new \Zend\Session\Container();
        
        if (! ($reponse instanceof \Core\Response)) {
            if (is_bool($type) || is_integer($type)) {
                $type = ($type) ? 'success' : 'danger';
            }
            
            $message = $reponse;
            $reponse = new \Core\Response();
            $reponse->setContent($message);
            $reponse->setType($type);
        }
        
        $session->response_message = $reponse;
        
        return $this;
    }

    /**
     *
     * @param mixed $key            
     * @param string $Value            
     * @return \Core\Controller\BaseController
     */
    function assign($key, $value = null)
    {
        if (is_object($key)) {
            if (method_exists($key, 'toArray')) {
                $key = $key->toArray();
            } elseif (method_exists($key, 'getArrayCopy')) {
                $key = $key->getArrayCopy();
            }
        }
        
        if (is_array($key)) {
            foreach ($key as $a => $b) {
                $this->viewVars[$a] = $b;
            }
        } else {
            $this->viewVars[$key] = $value;
        }
        
        return $this;
    }

    /**
     *
     * @return \Core\Controller\BaseController
     */
    function loadResponse()
    {
        $session = new \Zend\Session\Container();
        
        if (isset($session->response_message)) {
            $this->setResponse($session->response_message);
            $session->offsetUnset('response_message');
        }
        
        return $this;
    }

    /**
     *
     * @param strign|\Core\Response $reposnse            
     * @param strign|bool $type            
     * @return \Core\Controller\BaseController
     */
    function setResponse($reposnse, $type = null)
    {
        if ($reposnse instanceof \Core\Response) {
            $this->viewVars['response_message'] = $reposnse;
        } else {
            if (is_bool($type)) {
                $type = ($type) ? 'status' : 'error';
            }
            
            $oResponse = new \Core\Response();
            $oResponse->setContent($reposnse);
            $oResponse->setType($type);
            
            $this->viewVars['response_message'] = $oResponse;
        }
        
        return $this;
    }
}