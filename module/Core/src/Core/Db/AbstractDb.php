<?php
/**
 * Clase base para acceso a datos.
 * Proporciona métodos comunes de acceso a datos y eventos.
 * Todas las clases de acceso a datos deben extender de esta clase.  
 *  
 * @author yoterri@ideasti.com
 * @since 2014
 * 
 */
namespace Core\Db;

abstract class AbstractDb extends \Zend\Db\TableGateway\TableGateway
{

    /**
     *
     * @var string
     */
    const EVENT_PREFIXING = 'prefixing';

    /**
     *
     * @var string
     */
    const EVENT_BEFORE_INSERT = 'before_insert';

    /**
     *
     * @var string
     */
    const EVENT_AFTER_INSERT = 'after_insert';

    /**
     *
     * @var string
     */
    const EVENT_BEFORE_UPDATE = 'before_update';

    /**
     *
     * @var string
     */
    const EVENT_AFTER_UPDATE = 'after_update';

    /**
     *
     * @var string
     */
    const EVENT_BEFORE_DELETE = 'before_delete';

    /**
     *
     * @var string
     */
    const EVENT_AFTER_DELETE = 'after_delete';

    /**
     * Nombre de la tabla en la base de datos.
     * Se debe poner el nombre de la tabla sin prefijos
     *
     * @example nombre_tabla
     * @var string
     */
    protected $tableName = '';

    /**
     *
     * @var Nombre de la base de datos
     */
    protected $schemaName = '';

    /**
     *
     * @var string
     */
    protected $adapterKey;

    /**
     * Nombre de la clase que se debe usar como entidad para retornar los registros
     *
     * @example \Core\Entity\News
     * @var string
     */
    protected $entityClassName = '';

    /**
     *
     * @var \Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     *
     * @var \Zend\EventManager\EventInterface
     */
    protected $event;

    /**
     *
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $events;

    /**
     *
     * @param string $features            
     * @param \Zend\Db\ResultSet\ResultSetInterface $resultSetPrototype            
     * @param \Zend\Db\Sql\Sql $sql            
     */
    function __construct($features = null, \Zend\Db\ResultSet\ResultSetInterface $resultSetPrototype = null)
    {
        // process features
        if ($features !== null) {
            if ($features instanceof \Zend\Db\TableGateway\Feature\AbstractFeature) {
                $features = array(
                    $features
                );
            }
            
            if (is_array($features)) {
                $this->featureSet = new \Zend\Db\TableGateway\Feature\FeatureSet($features);
            } elseif ($features instanceof \Zend\Db\TableGateway\Feature\FeatureSet) {
                $this->featureSet = $features;
            } else {
                throw new \Exception('TableGateway expects $feature to be an instance of an AbstractFeature or a FeatureSet, or an array of AbstractFeatures');
            }
        } else {
            $this->featureSet = new \Zend\Db\TableGateway\Feature\FeatureSet();
        }
        
        // result prototype
        $this->resultSetPrototype = ($resultSetPrototype) ?  : new \Zend\Db\ResultSet\ResultSet();
    }

    /**
     * Set an event
     *
     * @param \Zend\EventManager\EventInterface $e            
     * @return void
     */
    public function setEvent(\Zend\EventManager\EventInterface $e)
    {
        $this->event = $e;
    }

    /**
     * Get the attached event
     *
     * Will create a new Event if none provided.
     *
     * @return \Zend\EventManager\EventInterface
     */
    public function getEvent()
    {
        if (! $this->event) {
            $this->setEvent(new \Zend\EventManager\Event());
        }
        
        return $this->event;
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param \Zend\EventManager\EventManagerInterface $events            
     * @return mixed
     */
    function setEventManager(\Zend\EventManager\EventManagerInterface $events)
    {
        $identifiers = array(
            __CLASS__,
            get_class($this)
        );
        if (isset($this->eventIdentifier)) {
            if ((is_string($this->eventIdentifier)) || (is_array($this->eventIdentifier)) || ($this->eventIdentifier instanceof Traversable)) {
                $identifiers = array_unique(array_merge($identifiers, (array) $this->eventIdentifier));
            } elseif (is_object($this->eventIdentifier)) {
                $identifiers[] = $this->eventIdentifier;
            }
            // silently ignore invalid eventIdentifier types
        }
        $events->setIdentifiers($identifiers);
        $this->events = $events;
        return $this;
    }

    /**
     *
     * @return \Zend\EventManagerEventManagerInterface
     */
    function getEventManager()
    {
        if (! $this->events instanceof \Zend\EventManager\EventManagerInterface) {
            $this->setEventManager(new \Zend\EventManager\EventManager());
        }
        return $this->events;
    }

    /**
     * Este método es llamado de forma automática en la seccion 'initializers' del archivo de configuración del módulo
     *
     * @param \Zend\Db\Adapter\AdapterInterface $adapter            
     * @return \Core\Db\AbstractDb
     */
    function setAdapter(\Zend\Db\Adapter\AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * Retorna el nombre de la clase de entidad que se haya configurado
     *
     * @return string
     */
    function getEntityClassName()
    {
        return $this->entityClassName;
    }

    /**
     *
     * @param \Zend\Stdlib\Hydrator\HydratorInterface $hydrator            
     * @return \Core\Db\AbstractDb
     */
    function setHydrator(\Zend\Stdlib\Hydrator\HydratorInterface $hydrator)
    {
        $this->hydrator;
        return $this;
    }

    /**
     *
     * @return \Zend\Stdlib\Hydrator\HydratorInterface
     */
    function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Verifica que la instancia cumpla con todos los requerimientos.
     * Se encarga de configurar el prefijo a los nombres de las tablas
     *
     * Este método es llamado de forma atumática en la seccion 'initializers' del archivo de configuración del módulo.
     *
     * @see \Zend\Db\TableGateway\AbstractTableGateway::initialize()
     */
    function initialize()
    {
        if ($this->isInitialized)
            return;
        
        $event = $this->getEvent();
        $event->setTarget($this);
        
        $this->getEventManager()->trigger(AbstractDb::EVENT_PREFIXING, $event);
        $prefix = $event->getParam('prefix');
        
        $this->table = new \Zend\Db\Sql\TableIdentifier("{$prefix}{$this->tableName}", $this->schemaName);
        
        // Sql object (factory for select, insert, update, delete)
        $this->sql = new \Zend\Db\Sql\Sql($this->getAdapter(), $this->table);
        
        $this->hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();
        
        // check sql object bound to same table
        if ($this->sql->getTable() != $this->table) {
            throw new \Exception('The table inside the provided Sql object must match the table of this TableGateway');
        }
        
        return parent::initialize();
    }

    /**
     *
     * @param array $data            
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    function doInsert(array $data)
    {
        $event = $this->getEvent();
        $event->setTarget($this);
        
        $event->setParam('data', $data);
        
        $this->getEventManager()->trigger(AbstractDb::EVENT_BEFORE_INSERT, $event);
        $data = $event->getParam('data');
        
        if (! $event->propagationIsStopped()) {
            
            $sql = $this->getSql();
            $insert = $sql->insert();
            $insert->values($data);
            
            $statement = $sql->prepareStatementForSqlObject($insert);
            
            $result = $statement->execute();
            
            $event->setParams(array());
            $event->setParam('result', $result);
            $this->getEventManager()->trigger(AbstractDb::EVENT_AFTER_INSERT, $event);
            $result = $event->getParam('result');
        } else {
            $result = new \Zend\Db\ResultSet\ResultSet();
        }
        
        return $result;
    }

    /**
     *
     * @param array $data            
     * @param string|array|closure $where            
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    function doUpdate(array $data, $where)
    {
        $event = $this->getEvent();
        $event->setTarget($this);
        
        $event->setParam('data', $data);
        $event->setParam('where', $where);
        
        $this->getEventManager()->trigger(AbstractDb::EVENT_BEFORE_UPDATE, $event);
        $data = $event->getParam('data');
        $where = $event->getParam('where');
        
        if (! $event->propagationIsStopped()) {
            
            $sql = $this->getSql();
            $update = $sql->update();
            $update->set($data)->where($where);
            
            $statement = $sql->prepareStatementForSqlObject($update);
            
            $result = $statement->execute();
            
            $event->setParams(array());
            $event->setParam('result', $result);
            $this->getEventManager()->trigger(AbstractDb::EVENT_AFTER_UPDATE, $event);
            $result = $event->getParam('result');
        } else {
            $result = new \Zend\Db\ResultSet\ResultSet();
        }
        
        return $result;
    }

    /**
     *
     * @param string|array|closure $where            
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    function doDelete($where)
    {
        $event = $this->getEvent();
        $event->setTarget($this);
        
        $event->setParam('where', $where);
        
        $this->getEventManager()->trigger(AbstractDb::EVENT_BEFORE_DELETE, $event);
        $where = $event->getParam('where');
        
        if (! $event->propagationIsStopped()) {
            
            $sql = $this->getSql();
            $delete = $sql->delete();
            $delete->where($where);
            
            $statement = $sql->prepareStatementForSqlObject($delete);
            
            $result = $statement->execute();
            
            $event->setParams(array());
            $event->setParam('result', $result);
            $this->getEventManager()->trigger(AbstractDb::EVENT_AFTER_DELETE, $event);
            $result = $event->getParam('result');
        } else {
            $result = new \Zend\Db\ResultSet\ResultSet();
        }
        
        return $result;
    }
    
    // $where = null, $cols = Zend_Db_Select::SQL_WILDCARD, $order = null, $count = null, $offset = null
    
    /**
     *
     * @param Where|\Closure|string|array|Predicate\PredicateInterface $where            
     * @param array $cols            
     * @param string $order            
     * @param int $count            
     * @param int $offset            
     * @return null ResultSetInterface
     * @throws \RuntimeException
     */
    function findBy($where = null, array $cols = array(), $order = null, $count = null, $offset = null)
    {
        $sql = $this->getSql();
        $select = $sql->select();
        
        if ($where)
            $select->where($where);
        
        if ($cols)
            $select->columns($cols);
        
        if ($order)
            $select->order($order);
        
        if ($offset)
            $select->offset($offset);
        
        if ($count)
            $select->limit($count);
        
        return $this->selectWith($select);
    }

    /**
     *
     * @param \Zend\Db\Sql\SqlInterface $sql
     *            Consulta sql a mostrar
     * @param string $exit
     *            Indica si se debe detener la ejecucion del código
     */
    function debugSql(\Zend\Db\Sql\SqlInterface $sql, $exit = true)
    {
        $str = $sql->getSqlString($this->getAdapter()
            ->getPlatform());
        
        // echo '<pre>';
        echo $str;
        // echo '</pre>';
        
        if ($exit)
            exit();
    }

    /**
     *
     * @return string
     */
    function getAdpaterKey()
    {
        return $this->adapterKey;
    }
}