<?php
namespace Core\Model;

abstract class AbstractModel
{

    /**
     *
     * @var \Core\Response
     */
    protected $response;

    /**
     *
     * @var \Zend\EventManager\EventInterface
     */
    protected $event;

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $dbAdapter;

    /**
     *
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator = null;

    /**
     *
     * @param \Zend\EventManager\EventInterface $e
     */
    function setEvent(\Zend\EventManager\EventInterface $e)
    {
        $this->event = $e;
    }

    /**
     *
     * @return \Zend\EventManager\EventInterface
     */
    function getEvent()
    {
        if (! $this->event) {
            $this->setEvent(new \Zend\EventManager\Event());
        }

        return $this->event;
    }

    /**
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     *
     * @param \Zend\Db\Adapter\Adapter $adapter
     */
    function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter)
    {
        $this->dbAdapter = $adapter;
        return $this;
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    /**
     *
     * @param mixed $content
     * @param string $type
     *            (success|error|warning|info)
     * @return \Core\ResposneTrait
     */
    function setResponse($content, $type = 'status')
    {
        $this->getResponse()
            ->setType($type)
            ->setContent($content);

        return $this;
    }

    /**
     *
     * @return \Core\Response
     */
    function getResponse()
    {
        if (! $this->response instanceof \Core\Response)
            $this->response = new \Core\Response();

        return $this->response;
    }

    /**
     *
     * @return \Core\ResposneTrait
     */
    function resetResponse()
    {
        $this->getResponse()->setContent(null);
        return $this;
    }

    /**
     *
     * @param Exception $e
     * @return \Core\Model
     */
    function setException(\Exception $e)
    {
        // $message = sprintf('<pre>%s</pre>', $e);
        // $message = 'There was an unexpected error, please try again.';
        $message = "<pre>$e</pre>";
        $this->setResponse($message, 'error');

        return $this;
    }

    /**
     *
     * @param array|object $fields
     * @param
     *            $params
     *
     * @return bool
     */
    function hasEmptyValues(array $required = array(), &$params)
    {
        $isObject = is_object($params);

        $flag = false;
        if (count($required)) {
            foreach ($required as $item) {
                if (($isObject && isset($params->$item)) || isset($params[$item])) {
                    $field = $isObject ? $params->$item : $params[$item];
                    if ((is_string($field) && '' == $field) || (is_array($field)) && 0 == count($field)) {
                        $flag = true;
                        break;
                    }
                } else {
                    $flag = true;
                    break;
                }
            }
        } else {
            $flag = true;
            break;
        }

        if ($flag) {
            $this->setResponse('Please fill in all required fields.', 'error');
        }

        return $flag;
    }

}