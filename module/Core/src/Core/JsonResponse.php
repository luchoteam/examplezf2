<?php
/**
 * @author <yoterri@ideasti.com>
 * @copyright 2014
 */
namespace Core;

class JsonResponse
{

    /**
     *
     * @var bool
     */
    protected $_success = false;

    /**
     *
     * @var array
     */
    protected $_data = array();

    /**
     *
     * @var array
     */
    protected $_errors = array();

    /**
     *
     * @return boolean
     *
     */
    public function getSuccess()
    {
        return $this->_success;
    }

    /**
     *
     * @param boolean $success            
     * @return \Core\JsonResponse
     */
    public function setSuccess($success)
    {
        $this->_success = $success;
        return $this;
    }

    /**
     *
     * @return array
     *
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     *
     * @return \Core\JsonResponse
     */
    function clearData()
    {
        $this->_data = array();
        return $this;
    }

    /**
     *
     * @param array $data            
     * @return \Core\JsonResponse
     */
    public function setData(array $data)
    {
        $this->_data = $data;
        $this->clearErrors();
        $this->setSuccess(true);
        return $this;
    }

    /**
     *
     * @param array $data            
     * @return \Core\JsonResponse
     */
    public function mergeData(array $data)
    {
        $data = array_merge($this->getData(), $data);
        $this->setData($data);
        return $this;
    }

    /**
     *
     * @param string $key            
     * @param mixed $value            
     * @return \Core\JsonResponse
     */
    function addData($value, $key = null)
    {
        $data = array();
        if(!empty($key))
            $data[$key] = $value;
        else
            $data = $value;
        
        $this->mergeData($data);
        
        return $this;
    }
    
    /**
     * @param array $errors
     * @return \Core\JsonResponse
     */
    function setError(array $errors)
    {
        $this->_errors = $errors;
        $this->clearData();
        $this->setSuccess(false);
        return $this;
    }

    /**
     *
     * @param string $message            
     * @param string $key            
     * @return \Core\JsonResponse
     */
    function addError($message, $key = null)
    {
        $this->setSuccess(false);
        $this->clearData();
        
        if(!empty($key))
        {
            if(! isset($this->_errors[$key]))
            {
                $this->_errors[$key] = array();
            }
            
            $this->_errors[$key][] = $message;
        }
        else 
        {
            $this->_errors[] = $message;
        }
        
        return $this;
    }

    /**
     *
     * @return array
     */
    function getErrors()
    {
        return $this->_errors;
    }

    /**
     *
     * @return \Core\JsonResponse
     */
    function clearErrors()
    {
        $this->_errors = array();
        return $this;
    }

    /**
     *
     * @return string
     */
    function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     *
     * @return array
     */
    function toArray()
    {
        $success = $this->getSuccess();
        if($success)
        {
            $key = 'data';
            $data = $this->getData();
        }
        else
        {
            $key = 'errors';
            $data = $this->getErrors();
        }
        
        return array(
            'success' => $success,
            $key => $data
        );
    }
}   