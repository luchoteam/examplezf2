<?php
namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Message extends AbstractHelper
{

    /**
     *
     * @param strign|\Core\Response $reposnse            
     * @param strign|bool $type            
     */
    public function __invoke($message = null, $type = null)
    {
        if(empty($message))
        {
            $message = $this->view->response_message;
        }
        
        if ($message instanceof \Core\Response) {
            $type = $message->getType();
            $message = $message->getContent();
        } else {
            if (is_bool($type)) {
                $type = ($type) ? 'success' : 'danger';
            }
        }
        
        $r = '';
        
        if(!empty($message))
        {
            $r .= sprintf('<div class="alert alert-%s">', $type);
            $r .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $r .= $message;
            $r .= '</div>';
        }
        
        return $r;
    }
}