<?php
namespace Core;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(\Zend\Mvc\MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $sm = $e->getApplication()->getServiceManager();
        
        // php configration settings
        $config = $sm->get('config');
        if (isset($config['phpSettings'])) {
            foreach ($config['phpSettings'] as $key => $value) {
                ini_set($key, $value);
            }
        }
        
        // set tableGateway prefix
        $eventManager->getSharedManager()->attach('Core\Db\AbstractDb', 'prefixing', function (\Zend\EventManager\Event $e) use($config)
        {
            if (isset($config['core_config']) && isset($config['core_config']['db_prefix'])) {
                $e->setParam('prefix', $config['core_config']['db_prefix']);
            }
        });
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
