<?php
return array(
    'core_config' => array(
        'db_prefix' => ''
    ),
    'controllers' => array(
        'abstract_factories' => array(
            'Core\Service\ControllerFactory'
        )
    ),
    'service_manager' => array(
        'invokables' => array(),
        'abstract_factories' => array(
            'Core\Service\CommonFactory'
        ),
        'initializers' => array(
            function ($instance,\Zend\ServiceManager\ServiceManager $sm)
            {
                if ($instance instanceof \Core\Model\AbstractModel) {
                    $instance->setServiceLocator($sm);
                    
                    $adapter = $sm->get('adapter');
                    $instance->setDbAdapter($adapter);
                }
                
                if ($instance instanceof \Core\Db\AbstractDb) {
                    $adapterKey = $instance->getAdpaterKey();
                    
                    if ($adapterKey) {
                        $adapter = $sm->get($adapterKey);
                    } else {
                        $adapter = $sm->get('adapter');
                    }
                    
                    $instance->setAdapter($adapter);
                    $instance->initialize();
                    
                    $entityClassName = $instance->getEntityClassName();
                    if ($entityClassName) {
                        $instance->getResultSetPrototype()->setArrayObjectPrototype($sm->get($entityClassName));
                    }
                }
            }
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'message' => 'Core\View\Helper\Message'
        )
    )
);
